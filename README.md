# Startup Language C

# C language #

C is a general-purpose, structured, imperative, procedural compiled programming language.

C é uma das linguagens de programação mais popularese existem poucas arquiteturas para as quais não existem compiladores para C. C tem influenciado muitas outras linguagens de programação, mais notavelmente C++, que originalmente começou como uma extensão para C.

https://gitlab.com/rodolfod2r2/startup-language-c

https://github.com/rodolfod2r2/C


### Linguagem C Básico - Conteúdo ###
 
- 001° **Introdução**
- 002° **Declaração de Variáveis**
- 003° **Printf**
- 004° **Scanf**
- 005° **Operadores de Atribuição**
- 006° **Constantes**
- 007° **Operadores Aritméticos**
- 008° **Comentários**
- 009° **Pré e Pós Incremento**
- 010° **Atribuição Simplificada**
- 011° **Operadores Relacionais**
- 012° **Operadores Lógicos**
- 013° **Comando If**
- 014° **Comando Else**
- 015° **Aninhamento IfElse**
- 016° **Operador Ternário(?)**
- 017° **Comando Switch**
- 018° **Comando While**
- 019° **Comando For**
- 020° **Comando Do<->While**
- 021° **Aninhamento de Repetições**
- 022° **Comando Break**
- 023° **Comando Continue**
- 024° **Comando Goto**
- 025° **Array / Vetor**
- 026° **Array / Matriz**
- 027° **Array Multidimensional**

### Linguagem C Estrutura de Dados - Conteúdo ###
 
- 001° **Introdução**
- 002° **Listas**
- 003° **Listas Ordenadas**
- 004° **Pilhas**
- 005° **Filas**
- 006° **Deques**
- 007° **Pesquisa de Dados**
- 008° **Árvores**
- 009° **Árvore Binária**
- 010° **Árvore Binária de Pesquisa**
- 011° **Árvore AVL**
- 012° **Indexação**
- 013° **Hashing**
- 014° **Árvore-B**
- 015° **Classificação de Dados**

#### Exemplos: ####

Exemplos:

https://gitlab.com/rodolfod2r2/startup-language-c

https://github.com/rodolfod2r2/C

#### Este Repositório ainda Esta em Edição  ####

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/rodolfod2r2/startup-language-c.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:ae8660ad4a7f17451501a0b1125dfb7c?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

